from flask import Flask
from flask import render_template, request, redirect, url_for, jsonify
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]


def resetID():
    for i in range(len(books)):
        books[i]['id'] = i+1


@app.route('/book/JSON')
def bookJSON():
    return json.dumps(books, indent=4)


@app.route('/')
@app.route('/book')
def showBook():
    return render_template('index.html', books=books)


@app.route('/book/<int:id>/edit', methods=('GET', 'POST'))
def editBook(id):
    book = books[id - 1]

    if request.method == 'POST':
        book['title'] = request.form['title']

        return redirect(url_for('showBook'))

    return render_template('edit.html', book=book)


@app.route('/book/<int:id>/delete', methods=('GET', 'POST'))
def deleteBook(id):
    book = books[id - 1]

    if request.method == 'POST':
        books.remove(book)
        resetID()

        return redirect(url_for('showBook'))

    return render_template('delete.html', book=book)


@app.route('/book/new', methods=('GET', 'POST'))
def newBook():
    if request.method == 'POST':
        title = request.form['title']
        books.append({'title': title, 'id': 7})
        resetID()

        return redirect(url_for('showBook'))

    return render_template('new.html')


if __name__ == '__main__':
    app.debug = True
    app.run()
